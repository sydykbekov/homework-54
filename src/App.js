import React, {Component} from 'react';
import './App.css';
import Card from './card';

class App extends Component {
    state = {
        array: [],
        bestHand: ''
    };

    handleClick = () => {
        let pokerHand = [];
        let bestHand;
        let array = [];
        let arrOfRank = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
        let arrOfSuit = ['diams', 'hearts', 'clubs', 'spades'];
        let arrOfSuitIcons = ['♦', '♥', '♣', '♠'];
        while (array.length < 5) {
            let identical;
            let randomRank = Math.floor(Math.random() * 13);
            let randomSuit = Math.floor(Math.random() * 4);

            let obj = {
                className: `card rank-${arrOfRank[randomRank].toLowerCase()} ${arrOfSuit[randomSuit]}`,
                suit: arrOfSuitIcons[randomSuit],
                rank: arrOfRank[randomRank]
            };

            array.forEach(function (item) {
                if (item.className === obj.className) {
                    identical = 'identicalCard';
                }
            });
            if (identical !== 'identicalCard') {
                array.push(obj);
                pokerHand.push(obj.rank + obj.suit);
            }
        }

        if ((pokerHand.includes('10♠') && pokerHand.includes('J♠') && pokerHand.includes('Q♠') && pokerHand.includes('K♠') && pokerHand.includes('A♠')) ||
            (pokerHand.includes('10♦') && pokerHand.includes('J♦') && pokerHand.includes('Q♦') && pokerHand.includes('K♦') && pokerHand.includes('A♦')) ||
            (pokerHand.includes('10♣') && pokerHand.includes('J♣') && pokerHand.includes('Q♣') && pokerHand.includes('K♣') && pokerHand.includes('A♣')) ||
            (pokerHand.includes('10♥') && pokerHand.includes('J♥') && pokerHand.includes('Q♥') && pokerHand.includes('K♥') && pokerHand.includes('A♥'))) {
            bestHand = 'Royal Flush';
            console.log(array);
            console.log(bestHand);
        }

        if (bestHand === undefined) {
            if ((array[0].suit === array[1].suit) && (array[0].suit === array[2].suit) && (array[0].suit === array[3].suit) && (array[0].suit === array[4].suit)) {
                pokerHand = pokerHand.map(function(i) {
                    return i.substring(0, i.length - 1);
                });
                if ((pokerHand.includes('A') && pokerHand.includes('2') && pokerHand.includes('3') && pokerHand.includes('4') && pokerHand.includes('5')) ||
                    (pokerHand.includes('2') && pokerHand.includes('3') && pokerHand.includes('4') && pokerHand.includes('5') && pokerHand.includes('6')) ||
                    (pokerHand.includes('3') && pokerHand.includes('4') && pokerHand.includes('5') && pokerHand.includes('6') && pokerHand.includes('7')) ||
                    (pokerHand.includes('4') && pokerHand.includes('5') && pokerHand.includes('6') && pokerHand.includes('7') && pokerHand.includes('8')) ||
                    (pokerHand.includes('5') && pokerHand.includes('6') && pokerHand.includes('7') && pokerHand.includes('8') && pokerHand.includes('9')) ||
                    (pokerHand.includes('6') && pokerHand.includes('7') && pokerHand.includes('8') && pokerHand.includes('9') && pokerHand.includes('10')) ||
                    (pokerHand.includes('7') && pokerHand.includes('8') && pokerHand.includes('9') && pokerHand.includes('10') && pokerHand.includes('J')) ||
                    (pokerHand.includes('8') && pokerHand.includes('9') && pokerHand.includes('10') && pokerHand.includes('J') && pokerHand.includes('Q')) ||
                    (pokerHand.includes('9') && pokerHand.includes('10') && pokerHand.includes('J') && pokerHand.includes('Q') && pokerHand.includes('K')) ||
                    (pokerHand.includes('10') && pokerHand.includes('J') && pokerHand.includes('Q') && pokerHand.includes('K') && pokerHand.includes('A'))) {
                    bestHand = 'Straight Flush';
                    console.log(array);
                    console.log(bestHand);
                }
            }
        }
        if (bestHand === undefined) {
            if ((pokerHand.includes('2♠') && pokerHand.includes('2♦') && pokerHand.includes('2♣') && pokerHand.includes('2♥')) ||
                (pokerHand.includes('3♠') && pokerHand.includes('3♦') && pokerHand.includes('3♣') && pokerHand.includes('3♥')) ||
                (pokerHand.includes('4♠') && pokerHand.includes('4♦') && pokerHand.includes('4♣') && pokerHand.includes('4♥')) ||
                (pokerHand.includes('5♠') && pokerHand.includes('5♦') && pokerHand.includes('5♣') && pokerHand.includes('5♥')) ||
                (pokerHand.includes('6♠') && pokerHand.includes('6♦') && pokerHand.includes('6♣') && pokerHand.includes('6♥')) ||
                (pokerHand.includes('7♠') && pokerHand.includes('7♦') && pokerHand.includes('7♣') && pokerHand.includes('7♥')) ||
                (pokerHand.includes('8♠') && pokerHand.includes('8♦') && pokerHand.includes('8♣') && pokerHand.includes('8♥')) ||
                (pokerHand.includes('9♠') && pokerHand.includes('9♦') && pokerHand.includes('9♣') && pokerHand.includes('9♥')) ||
                (pokerHand.includes('10♠') && pokerHand.includes('10♦') && pokerHand.includes('10♣') && pokerHand.includes('10♥')) ||
                (pokerHand.includes('J♠') && pokerHand.includes('J♦') && pokerHand.includes('J♣') && pokerHand.includes('J♥')) ||
                (pokerHand.includes('Q♠') && pokerHand.includes('Q♦') && pokerHand.includes('Q♣') && pokerHand.includes('Q♥')) ||
                (pokerHand.includes('K♠') && pokerHand.includes('K♦') && pokerHand.includes('K♣') && pokerHand.includes('K♥')) ||
                (pokerHand.includes('A♠') && pokerHand.includes('A♦') && pokerHand.includes('A♣') && pokerHand.includes('A♥'))) {
                bestHand = 'Four of a kind';
                console.log(array);
                console.log(bestHand);
            }
        }

        if (bestHand === undefined) {
            if ((array[0].suit === array[1].suit) && (array[0].suit === array[2].suit) && (array[0].suit === array[3].suit) && (array[0].suit === array[4].suit)) {
                bestHand = 'Flush';
                console.log(array);
                console.log(bestHand);
            }
        }

        if (bestHand === undefined) {
            if ((pokerHand.includes('A') && pokerHand.includes('2') && pokerHand.includes('3') && pokerHand.includes('4') && pokerHand.includes('5')) ||
                (pokerHand.includes('2') && pokerHand.includes('3') && pokerHand.includes('4') && pokerHand.includes('5') && pokerHand.includes('6')) ||
                (pokerHand.includes('3') && pokerHand.includes('4') && pokerHand.includes('5') && pokerHand.includes('6') && pokerHand.includes('7')) ||
                (pokerHand.includes('4') && pokerHand.includes('5') && pokerHand.includes('6') && pokerHand.includes('7') && pokerHand.includes('8')) ||
                (pokerHand.includes('5') && pokerHand.includes('6') && pokerHand.includes('7') && pokerHand.includes('8') && pokerHand.includes('9')) ||
                (pokerHand.includes('6') && pokerHand.includes('7') && pokerHand.includes('8') && pokerHand.includes('9') && pokerHand.includes('10')) ||
                (pokerHand.includes('7') && pokerHand.includes('8') && pokerHand.includes('9') && pokerHand.includes('10') && pokerHand.includes('J')) ||
                (pokerHand.includes('8') && pokerHand.includes('9') && pokerHand.includes('10') && pokerHand.includes('J') && pokerHand.includes('Q')) ||
                (pokerHand.includes('9') && pokerHand.includes('10') && pokerHand.includes('J') && pokerHand.includes('Q') && pokerHand.includes('K')) ||
                (pokerHand.includes('10') && pokerHand.includes('J') && pokerHand.includes('Q') && pokerHand.includes('K') && pokerHand.includes('A'))) {
                bestHand = 'Straight';
                console.log(array);
                console.log(bestHand);
            }
        }

        this.setState({array, bestHand});
    };

    render() {
        return (
            <div className="App playingCards faceImages">
                <button onClick={this.handleClick} id="random">Shuffle Cards</button>
                <ul className="table">
                    {this.state.array.map((obj, i) => <Card class={obj.className} rank={obj.rank} suit={obj.suit} key={i}/>)}
                </ul>
                <p>{this.state.bestHand}</p>
            </div>
        );
    }
}


export default App;
