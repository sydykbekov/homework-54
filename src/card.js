import React from 'react';

const Card = props => (
    <div className={props.class}>
        <span className="rank">{props.rank}</span>
        <span className="suit">{props.suit}</span>
    </div>
);

export default Card;

